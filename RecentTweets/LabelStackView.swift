//
//  LabelStack.swift
//  RecentTweets
//
//  Created by Artemiy Sobolev on 21/02/2017.
//  Copyright © 2017 mipt. All rights reserved.
//

import UIKit

protocol LabelStackViewDelegate: class {
    func labelStackViewNewProtion(_ stackView: LabelStackView) -> [String]
    func labelStackViewTimeInterval(_ stackView: LabelStackView) -> TimeInterval
}

public
class LabelStackView: UIView {
    
    private var labels = [UILabel]()
    
    weak var delegate: LabelStackViewDelegate? {
        didSet {
            setup()
        }
    }
    
    var timer: Timer?
    
    func setup() {
        
        guard let ti = delegate?.labelStackViewTimeInterval(self) else {
            return
        }
        
        timer?.invalidate()
        
        timer = Timer.scheduledTimer(withTimeInterval: ti, repeats: true) { [weak self] _ in
            
            guard let strongSelf = self, let values = strongSelf.delegate?.labelStackViewNewProtion(strongSelf) else {
                return
            }
            
            strongSelf.setupLabels(for: values)
            strongSelf.addLabelAnimations(with: ti)
            
        }
    }
    
    private struct AnimationMetrics {
        static let perItemShift: TimeInterval = 0.05
        static let relativeAnimationDuration: TimeInterval = 0.2
        static let springDumping: CGFloat = 0.5
        static let springVelocity: CGFloat = 0.5
    }
    
    func addLabelAnimations(with timeInterval: TimeInterval) {
        let duration = AnimationMetrics.relativeAnimationDuration * timeInterval
        
        for i in 0..<labels.count {
            layoutLabel(at: i, bias: bounds.height)
            labels[i].alpha = 0
            let timeBias = TimeInterval(i) * timeInterval * AnimationMetrics.perItemShift
            
            let removeAnimationDelay = timeInterval - duration + timeBias
            
            UIView.animate(withDuration: duration, delay: timeBias, usingSpringWithDamping: AnimationMetrics.springDumping, initialSpringVelocity: AnimationMetrics.springVelocity, options: [], animations: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.layoutLabel(at: i)
                strongSelf.labels[i].alpha = 1
            })
            
            UIView.animate(withDuration: duration, delay: removeAnimationDelay + timeBias, usingSpringWithDamping: AnimationMetrics.springDumping, initialSpringVelocity: AnimationMetrics.springVelocity, options: [], animations: { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.layoutLabel(at: i, bias: -2 * strongSelf.bounds.height)
                strongSelf.labels[i].alpha = 1
            })
        }
    }
    
    func layoutLabel(at index: Int, bias: CGFloat = 0) {
        labels[index].frame = CGRect(x: bounds.minX, y: bias + bounds.minY + CGFloat(index) * bounds.height / CGFloat(labels.count), width: bounds.width, height: bounds.height / CGFloat(labels.count))
    }
    
    private func setupLabels(for values: [String]) {
        
        let delta = labels.count - values.count
        
        if delta > 0 {
            
            for i in (values.count..<labels.count).reversed() {
                labels[i].removeFromSuperview()
                labels.remove(at: i)
            }
            
        } else {
            
            var newLabels = [UILabel]()
            
            for _ in labels.count..<values.count {
                
                let newLabel = UILabel()
                newLabel.numberOfLines = 0
                newLabel.textAlignment = .center
                addSubview(newLabel)
                newLabels.append(newLabel)
            }
            
            labels += newLabels
            
        }
        
        zip(labels, values).forEach { $0.text = $1 }
        
    }
    
}

