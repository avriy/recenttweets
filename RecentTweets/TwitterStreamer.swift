//
//  TwitterStreamer.swift
//  RecentTweets
//
//  Created by Artemiy Sobolev on 21/02/2017.
//  Copyright © 2017 mipt. All rights reserved.
//

import Foundation
import Accounts
import Social

protocol TweetsStreamerDelegate: class {
    func tweetsStreamerHasNoTwitterAccess(_ streamer: TweetsStreamer)
    func tweetsStreamer(_ streamer: TweetsStreamer, didFaceError: Error)
    func tweetsStreamer(_ streamer: TweetsStreamer, didReceiveTweet: Tweet)
    func tweetsStreamer(_ streamer: TweetsStreamer, selectAccountFrom accounts: [ACAccount], selectionHandler: @escaping (ACAccount) -> Void)
}

final
class TweetsStreamer: NSObject, URLSessionDataDelegate {
    
    let accountStore = ACAccountStore()
    let bracketsValidator = BracketsValidator()
    
    weak var delegate: TweetsStreamerDelegate?
    private var account: ACAccount?
    
    lazy var session: URLSession = {
        return URLSession(configuration: .default, delegate: self, delegateQueue: nil)
    }()
    
    var searchString: String?
    private var task: URLSessionDataTask?
    
    private func fetchTweets(with word: String, for account: ACAccount) {
        searchString = word
        let request = SLRequest.tweetsTracking(word: word)
        request.account = account
        task?.cancel()
        task = session.dataTask(with: request.preparedURLRequest())
        task?.resume()
    }
    
    func tweetsMatching(word: String) {
        bracketsValidator.flush()
        
        if let account = account {
            return fetchTweets(with: word, for: account)
        }
        
        let accountType = accountStore.accountType(withAccountTypeIdentifier: ACAccountTypeIdentifierTwitter)
        
        accountStore.requestAccessToAccounts(with: accountType!, options: nil) { [weak self] (hasAccess, error) in
            DispatchQueue.main.async { [weak self] in
                
                guard let strongSelf = self else { return }
                
                if let error = error {
                    strongSelf.delegate?.tweetsStreamer(strongSelf, didFaceError: error)
                    return
                }
                
                guard hasAccess else {
                    strongSelf.delegate?.tweetsStreamerHasNoTwitterAccess(strongSelf)
                    return
                }
                
                guard let accounts = strongSelf.accountStore.accounts(with: accountType) as? [ACAccount] else { return }
                
                if accounts.count > 1 {
                    strongSelf.delegate?.tweetsStreamer(strongSelf, selectAccountFrom: accounts) { [weak self] account in
                        guard let strongSelf = self else { return }
                        strongSelf.account = account
                        strongSelf.fetchTweets(with: word, for: account)
                    }
                    return
                }
                
                guard let firstAccount = strongSelf.accountStore.accounts(with: accountType).first as? ACAccount else {
                    return
                }
                strongSelf.account = firstAccount
                strongSelf.fetchTweets(with: word, for: firstAccount)
            }
            
        }
    }
        
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        DispatchQueue.main.async { [weak self] in
            
            guard let strongSelf = self, dataTask == strongSelf.task else {
                return
            }
            
            guard let newPortionString = String(data: data, encoding: .utf8) else {
                return
            }
            strongSelf.bracketsValidator.add(string: newPortionString) { jsonString in
                do {
                    let json = try JSONSerialization.jsonObject(with: jsonString.data(using: .utf8)!, options: [])
                    let dictionary = json as? [AnyHashable : Any]
                    
                    if let tweet = dictionary.flatMap(Tweet.init(dictionary:)) {
                        strongSelf.delegate?.tweetsStreamer(strongSelf, didReceiveTweet: tweet)
                    }
                } catch let error {
                    strongSelf.delegate?.tweetsStreamer(strongSelf, didFaceError: error)
                    
                }
            }
        }
        
    }
}

extension Tweet {
    init?(dictionary: [AnyHashable : Any]) {
        guard let text = dictionary["text"] as? String else {
            return nil
        }
        self.text = text
    }
}
