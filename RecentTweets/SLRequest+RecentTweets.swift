//
//  SLRequest+RecentTweets.swift
//  RecentTweets
//
//  Created by Artemiy Sobolev on 21/02/2017.
//  Copyright © 2017 mipt. All rights reserved.
//

import Social

extension SLRequest {
    static let statusPath = "https://stream.twitter.com/1.1/statuses/filter.json"
    
    static func tweetsTracking(word: String) -> SLRequest {
        let statusURL = URL(string: SLRequest.statusPath)!
        return SLRequest(forServiceType: SLServiceTypeTwitter, requestMethod: .POST, url: statusURL, parameters: ["track" : word])!
    }
}
