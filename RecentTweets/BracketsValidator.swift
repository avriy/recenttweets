//
//  BracketsValidator.swift
//  RecentTweets
//
//  Created by Artemiy Sobolev on 23/02/2017.
//  Copyright © 2017 mipt. All rights reserved.
//

import Foundation

final
class BracketsValidator {
    
    private var string = ""
    private var openBrackets = 0
    private var closedBrackes = 0
    
    func flush() {
        string = ""
        openBrackets = 0
        closedBrackes = 0
    }
    
    func add(string s: String, handler: (String) -> Void) {
        var previousIndex = s.startIndex
        
        for index in s.characters.indices {
            
            let char = s.characters[index]
            if char == "{" {
                openBrackets += 1
            }
            if char == "}" {
                closedBrackes += 1
            }
            
            if openBrackets == closedBrackes {
                let newIndex = s.index(index, offsetBy: 1)
                let substring = s.substring(with: previousIndex..<newIndex)
                
                guard substring != "\r\n" else {
                    string = ""
                    previousIndex = newIndex
                    continue
                }
                
                handler(string + substring)
                string = ""
                previousIndex = newIndex
                
            }
            
        }
        
        string = string + s.substring(with: previousIndex..<s.endIndex)
    }
}
