//
//  ViewController.swift
//  RecentTweets
//
//  Created by Artemiy Sobolev on 20/02/2017.
//  Copyright © 2017 mipt. All rights reserved.
//

import UIKit
import Accounts

func ==(lhs: Tweet, rhs: Tweet) -> Bool {
    return lhs.text == rhs.text
}

struct Tweet: Hashable {
    let text: String
    
    var hashValue: Int {
        return text.hashValue
    }
}

class ViewController: UIViewController, TweetsStreamerDelegate, LabelStackViewDelegate {
    
    var streamer = TweetsStreamer()
    var tweets = [Tweet]()
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelStackView: LabelStackView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var displayButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        streamer.delegate = self
        labelStackView.delegate = self
    }
    
    @IBAction func searchChanged(_ sender: UITextField) {
        if let text = searchTextField.text, !text.isEmpty, text != streamer.searchString {
            displayButton.isEnabled = true
        } else {
            displayButton.isEnabled = false
        }
    }
    
    func labelStackViewNewProtion(_ stackView: LabelStackView) -> [String] {
        
        if tweets.count > 5 {
            let lastRecent = tweets[(tweets.count - 5)..<tweets.count]
            //  we keep only last 5 tweets
            tweets = Array(lastRecent)
        }
        
        return tweets.map({ $0.text })
    }
    
    func labelStackViewTimeInterval(_ stackView: LabelStackView) -> TimeInterval {
        return 2.5
    }
    
    @IBAction func searchButtonPressed(_ sender: UIButton) {
        tweets = []
        sender.isEnabled = false
        activityIndicator.startAnimating()
        streamer.tweetsMatching(word: searchTextField.text!)
        searchTextField.resignFirstResponder()
    }

    func tweetsStreamer(_ streamer: TweetsStreamer, didFaceError error: Error) {
        activityIndicator.stopAnimating()
        present(UIAlertController.errorHandler(for: error), animated: true)
    }

    func tweetsStreamerHasNoTwitterAccess(_ streamer: TweetsStreamer) {
        activityIndicator.stopAnimating()
        present(UIAlertController.noTwitter(), animated: true)
    }
    
    func tweetsStreamer(_ streamer: TweetsStreamer, didReceiveTweet tweet: Tweet) {
        activityIndicator.stopAnimating()
        tweets.append(tweet)
    }
    
    func tweetsStreamer(_ streamer: TweetsStreamer, selectAccountFrom accounts: [ACAccount], selectionHandler: @escaping (ACAccount) -> Void) {
        present(UIAlertController.selectAccount(from: accounts, handler: selectionHandler), animated: true)
    }
}

