//
//  AppDelegate.swift
//  RecentTweets
//
//  Created by Artemiy Sobolev on 20/02/2017.
//  Copyright © 2017 mipt. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }

}

