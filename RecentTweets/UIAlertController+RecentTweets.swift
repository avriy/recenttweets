//
//  UIAlertController+RecentTweets.swift
//  RecentTweets
//
//  Created by Artemiy Sobolev on 22/02/2017.
//  Copyright © 2017 mipt. All rights reserved.
//

import UIKit
import Accounts

extension UIAlertController {
    
    static func errorHandler(for error: Error) -> UIAlertController {
        let result = UIAlertController(title: "Error", message: (error as NSError).localizedDescription, preferredStyle: .alert)
        result.addAction(UIAlertAction(title: "OK", style: .default))
        return result
    }
    
    static func noTwitter() -> UIAlertController {
        let result = UIAlertController(title: "No access to Twitter", message: "Access to your twitter account is disabled", preferredStyle: .alert)
        result.addAction(UIAlertAction(title: "OK", style: .default))
        return result
    }
    
    
    static func selectAccount(from accounts: [ACAccount], handler: @escaping (ACAccount) -> Void) -> UIAlertController {
        
        let alert = UIAlertController(title: "Select Account", message: "Please select twitter account to continue", preferredStyle: .actionSheet)
        for account in accounts {
            
            let action = UIAlertAction(title: account.userFullName, style: .default) { _ in
                handler(account)
            }
            
            alert.addAction(action)
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        return alert
    }
    
}

